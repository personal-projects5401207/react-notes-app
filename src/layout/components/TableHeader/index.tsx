import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import React from "react";

function TableHeader(props: any) {
  let data = props.data;
  if (data.length === 0) {
    return null;
  }

  const keys = Object.keys(data[0]);

  return (
    <TableHead>
      <TableRow>
        {keys.map((key) => (
          <TableCell key={key}>{key}</TableCell>
        ))}
        <TableCell>Actions</TableCell>
      </TableRow>
    </TableHead>
  );
}

export default TableHeader;
