import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { del, get, post } from "../../../utils/api";
import TableHeader from "../TableHeader";
import { Button } from "@mui/material";
import AddNotesModal from "../AddNotesModal";
import SnackbarAlert from "../../../common/sanckbar";
import EditIcon from "@mui/icons-material/Edit";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import DeleteIcon from "@mui/icons-material/Delete";
import { Link, useNavigate } from "react-router-dom";

function ListModule(props: any) {
  const navigate = useNavigate();
  const [data, setData] = useState<any>([]);
  const [formData, setFormData] = useState<any>({
    title: "",
    content: "",
  });
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [snackbar, setSnackbar] = useState(false);
  const [alert_severity, setAlertSeverity] = useState("warning");
  const [alert_message, setMessage] = useState("");

  const alert = (message: string, alert_severity: string) => {
    setSnackbar(true);
    setMessage(message);
    setAlertSeverity(alert_severity);
  };

  const closeSnackbar = () => {
    // this.setState({ snackbar: false });
    setSnackbar(false);
  };

  useEffect(() => {
    getNotes();
  }, []);

  const getNotes = async () => {
    try {
      const response = await get("notes", {});
      console.log(response);
      setData(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    console.log("formData", formData);

    try {
      const response = await post("notes", formData);

      if (response) {
        console.log(response);
        alert("Note added successfully", "success");
        getNotes();
      }
    } catch (error) {
      alert("Error adding note", "error");
      console.log(error);
    }
    handleClose();
  };

  const handleChange = (e: any) => {
    console.log("changed");
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleDetails = (e: any) => {
    console.log("details", e);
    navigate(`/dashboard/${e._id}`);
  };

  const handleEdit = (e: any) => {
    console.log("edit", e);
    navigate(`/dashboard/edit/${e._id}`);
  };

  const handleDelete = async (e: any) => {
    console.log("delete", e);

    try {
      const response = await del("notes/" + e._id);

      if (response) {
        console.log(response);
        alert("Note deleted successfully", "success");
        getNotes();
      }
    } catch (error) {
      alert("Error deleting note", "error");
      console.log(error);
    }
  };

  return (
    <>
      <SnackbarAlert
        open={snackbar}
        closeSnackbar={closeSnackbar}
        message={alert_message}
        severity={alert_severity}
      />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHeader data={data} />
          <TableBody>
            {data.map((row: any) => (
              <TableRow
                key={row._id}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row._id}
                </TableCell>
                <TableCell>{row.title}</TableCell>
                <TableCell>{row.content}</TableCell>
                <TableCell>
                  <div>
                    <Button>
                      <Link to={props.detailLink + row._id}>
                        <LibraryBooksIcon />
                      </Link>
                    </Button>
                    <Button>
                      <Link to={props.editLink + row._id}>
                        <EditIcon />
                      </Link>
                    </Button>
                    <Button onClick={() => handleDelete(row)}>
                      <DeleteIcon />
                    </Button>
                  </div>
                </TableCell>
              </TableRow>
            ))}
            <TableRow>
              <Button onClick={handleOpen}>+ Add New Note</Button>
              <AddNotesModal
                open={open}
                handleClose={handleClose}
                handleSubmit={handleSubmit}
                handleChange={handleChange}
                formData={formData}
              />
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

export default ListModule;
