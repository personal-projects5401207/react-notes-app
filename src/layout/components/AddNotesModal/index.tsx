import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { useState } from "react";
import { TextField } from "@mui/material";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  //   border: "2px solid #000",
  boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1), 0 6px 20px rgba(0, 0, 0, 0.15)",
  borderRadius: 2,
  p: 4,
};

export default function AddNotesModal(props: any) {
  let data = props.formData;

  return (
    <div>
      <Modal
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <form onSubmit={props.handleSubmit}>
            <div className="mb-3">
              <TextField
                sx={{ width: "100% !important" }}
                fullWidth
                type="text"
                className="reg-input name-field"
                name="title"
                value={data.title}
                onChange={(e) => props.handleChange(e)}
                placeholder="Title"
              />
            </div>
            <div className="mb-3">
              <TextField
                sx={{ width: "100% !important" }}
                fullWidth
                type="text"
                className="reg-input name-field"
                name="content"
                multiline
                rows={4}
                value={data.content}
                onChange={(e) => props.handleChange(e)}
                placeholder="Content"
              />
            </div>
            <div className="d-flex justify-content-end">
              <Button variant="contained" type="submit">
                Submit
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
    </div>
  );
}
