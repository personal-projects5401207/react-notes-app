import React from "react";
import { Outlet } from "react-router-dom";
import Navbar from "../layout/components/Navbar";
import "./index.scss";

function MasterLayout() {
  return (
    <div>
      <Navbar
      //   user={state.user} onChange={setDropdownValue}
      />
      <div className="master-spacing">
        {/* {path.split("/").length - 1 === 4 && <BreadCrumb />} */}
        <Outlet />
      </div>
      {/* <Footer /> */}
    </div>
  );
}

export default MasterLayout;
