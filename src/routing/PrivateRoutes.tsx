import React, { useContext, useEffect } from "react";
import {
  Navigate,
  Route,
  Routes,
  useLocation,
  useNavigate,
} from "react-router-dom";
import MasterLayout from "../master-layout";
import { AppContext } from "../utils/state";
import { getProfile } from "../utils/api";
import Dashboard from "../modules/home/dashboard";
import Notes from "../modules/home/notes";

const PrivateRoutes = () => {
  const location = useLocation();
  const navigate = useNavigate();

  return (
    <Routes>
      <Route element={<MasterLayout />}>
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/notes/*" element={<Notes />} />
        <Route index element={<Navigate to="/notes/list" />} />
      </Route>
    </Routes>
  );
};

export { PrivateRoutes };
