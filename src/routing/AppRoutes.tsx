import React, { FC, useContext } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import App from "../App";
import { AppContext } from "../utils/state";
import Login from "../modules/login";
import Register from "../modules/register";
import { PrivateRoutes } from "./PrivateRoutes";

const AppRoutes: FC = () => {
  let accessToken = "" + localStorage.getItem("accessToken");
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<App />}>
          <Route path="/*" element={<PrivateRoutes />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export { AppRoutes };
