import React, { createContext, useReducer } from "react";


// const AppContext = createContext();

const initialState = {
    isAuthenticated:false,
}
// const AppContext = createContext();

const AppContext = createContext<any | undefined>({});
// state={count,isAuthenticated}

let reducer = (state: any, action: any) => {
    switch (action.type) {
        case "setAuth": {
            return { ...state, auth:action.data }
        }
    }
    return state;
};

function AppContextProvider(props: any) {
    const fullInitialState = {
        ...initialState,
    }
    let [state, dispatch] = useReducer(reducer, fullInitialState);
    let value = { state, dispatch };
    return (
        <AppContext.Provider value={value}>{props.children}</AppContext.Provider>
    );
}

let AppContextConsumer = AppContext.Consumer;

export { AppContext, AppContextProvider, AppContextConsumer };