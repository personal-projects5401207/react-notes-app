import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { get } from "../../../../utils/api";
import { Card, CardContent, Typography } from "@mui/material";

function Details() {
  let { id } = useParams();

  useEffect(() => {
    getNoteDetails(id);
  }, [id]);

  const [data, setData] = useState<any>({});

  const getNoteDetails = async (id: any) => {
    try {
      const response = await get(`/notes/${id}`, {});
      console.log("responseData", response);
      setData(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Card>
      <CardContent>
        <Typography variant="h5" component="div" sx={{ marginBottom: 3 }}>
          Note Details
        </Typography>
        <Typography variant="body2" sx={{ marginBottom: 2 }}>
          {" "}
          ID: {id}
        </Typography>
        <Typography variant="body2" sx={{ marginBottom: 2 }}>
          {" "}
          Title: {data.title}{" "}
        </Typography>
        <Typography variant="body2"> Content: {data.content} </Typography>
      </CardContent>
    </Card>
  );
}

export default Details;
