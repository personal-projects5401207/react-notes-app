import React from "react";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import Details from "./Details/details";
import List from "./List";
import Edit from "./Edit/edit";

function Notes() {
  return (
    <Routes>
      <Route
        element={
          <>
            <Outlet />
          </>
        }
      >
        <Route path="/list" element={<List />} />

        <Route
          path="edit/:id"
          element={
            <>
              <Edit />
            </>
          }
        />

        <Route
          path="details/:id"
          element={
            <>
              <Details />
            </>
          }
        />

        <Route index element={<Navigate to="/notes/list" />} />
      </Route>
    </Routes>
  );
}

export default Notes;
