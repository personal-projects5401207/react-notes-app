import React from "react";
import ListModule from "../../../../layout/components/List";
import "./index.scss";

function List() {
  return <ListModule detailLink="/notes/details/" editLink="/notes/edit/" />;
}

export default List;
