import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { get, put } from "../../../../utils/api";
import {
  Button,
  Card,
  CardContent,
  TextField,
  Typography,
} from "@mui/material";
import SnackbarAlert from "../../../../common/sanckbar";

function Edit() {
  const navigate = useNavigate();
  let { id } = useParams();

  useEffect(() => {
    getNoteDetails(id);
  }, [id]);

  const [data, setData] = useState<any>({});

  const [snackbar, setSnackbar] = useState(false);
  const [alert_severity, setAlertSeverity] = useState("warning");
  const [alert_message, setMessage] = useState("");

  const alert = (message: string, alert_severity: string) => {
    setSnackbar(true);
    setMessage(message);
    setAlertSeverity(alert_severity);
  };

  const closeSnackbar = () => {
    // this.setState({ snackbar: false });
    setSnackbar(false);
  };

  const getNoteDetails = async (id: any) => {
    try {
      const response = await get(`/notes/${id}`, {});
      console.log("responseData", response);
      setData(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleChange = (e: any) => {
    console.log("changed");
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();

    console.log("formDataUpdate", data);

    try {
      const response = await put(`/notes/${id}`, data);

      if (response) {
        console.log(response);
        alert("Note updated successfully", "success");
        navigate(`/notes/list`);
      }
    } catch (error) {
      alert("Error updating note", "error");
      console.log(error);
    }
  };

  return (
    <>
      <SnackbarAlert
        open={snackbar}
        closeSnackbar={closeSnackbar}
        message={alert_message}
        severity={alert_severity}
      />
      <Card>
        <CardContent>
          <Typography variant="h5" component="div" sx={{ marginBottom: 3 }}>
            Note Details
          </Typography>
          <Typography variant="body2" sx={{ marginBottom: 2 }}>
            ID: {id}
          </Typography>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <TextField
                sx={{ width: "100% !important" }}
                fullWidth
                type="text"
                className="reg-input name-field"
                name="title"
                value={data.title}
                onChange={(e) => handleChange(e)}
                placeholder="Title"
              />
            </div>
            <div className="mb-3">
              <TextField
                sx={{ width: "100% !important" }}
                fullWidth
                type="text"
                className="reg-input name-field"
                name="content"
                multiline
                rows={4}
                value={data.content}
                onChange={(e) => handleChange(e)}
                placeholder="Content"
              />
            </div>
            <div className="d-flex justify-content-end">
              <Button variant="contained" type="submit">
                Submit
              </Button>
            </div>
          </form>
        </CardContent>
      </Card>
    </>
  );
}

export default Edit;
